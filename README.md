# images

Building bootable images used by Glasklar Teknik AB.

For ST images we use
[stimages](https://git.glasklar.is/system-transparency/core/stimages). We
defer signing of the images to a separate signing process, see
TBD. Signed ST images are being published on https://st.glasklar.is/.

For RPI images we use the Debian Raspberry PI Maintainers'
[image-spec](https://salsa.debian.org/raspi-team/image-specs).
See
[rpi-build-image.sh](https://git.glasklar.is/glasklar/services/misc/-/blob/main/RPI/rpi-build-image.sh)
for how to.

For key material provisioning images we use
[keykeeper](https://git.glasklar.is/glasklar/trust/keykeeper).
