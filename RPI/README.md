# Building Raspberry PI images

[rpi-build.image.sh](./rpi-build.image.sh) uses Debian Raspberry PI Maintainers'
[image-spec][] to clone the image-specs repo into $PWD and build an
image from a [vmdb2][] specification file.

We keep image specification files and configuration data under [config](./config).

The resulting disk image file is created as ./image-specs/CONFIGURATION.img.

[image-spec]: https://salsa.debian.org/raspi-team/image-specs
[vmdb2]: https://vmdb2-manual.liw.fi/

## Debugging

Inspecting an image can be done by loopback mounting it. Use your
favourite disk editor (parted) to find at which offsets in the image
the two filesystems start.

Example:

    root@debian12:~# parted $(losetup -f --show ~builder/usr/src/images/RPI/image-specs/witness.img) unit kib p
    Model: Loopback device (loopback)
    Disk /dev/loop0: 2560000kiB
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags:

    Number  Start      End         Size        Type     File system  Flags
     1      4096kiB    524288kiB   520192kiB   primary  fat16        lba
     2      524288kiB  2560000kiB  2035712kiB  primary  ext4

    root@debian12:~# losetup -d /dev/loop0
    root@debian12:~# mount -o ro,loop,offset=524288kiB ~builder/usr/src/images/RPI/image-specs/witness.img /mnt
    root@debian12:~# mount -o ro,loop,offset=4096kiB ~builder/usr/src/images/RPI/image-specs/witness.img /mnt/boot/firmware
    mount: /mnt/boot/firmware: overlapping loop device exists for /srv/builder/usr/src/images/RPI/image-specs/witness.img.
    root@debian12:~# mount -o ro $(losetup -o 4096kiB --show -f ~builder/usr/src/images/RPI/image-specs/witness.img) /mnt/boot/firmware
    root@debian12:~# cat /mnt/boot/firmware/cmdline.txt
    console=tty0 console=ttyS1,115200 root=LABEL=RASPIROOT rw fsck.repair=yes net.ifnames=0 rootwait
    root@debian12:~# umount -Rd /mnt
