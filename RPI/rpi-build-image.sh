#! /bin/bash
set -eu

# Build an RPI image using https://salsa.debian.org/raspi-team/image-specs.
#
# Input is a vmdb2 specification file.
# BUG: The vmdb2 file name must be on the form NAME.vmdb2.
# A boilerplate specification file can be generated using the image-specs Makefile.
#
# vmdb2 is run under fakemachine to not require uid0. If the user is
# not in group kvm and fakemachine doesn't realise that KVM won't
# work, your can try --backend=qemu.
#
# Tested on Debian 12.8 amd64.

declare -i DO_PREPARE_SYSTEM=0; [[ "$1" == "-s" ]] && { DO_PREPARE_SYSTEM=1; shift; }
declare -r IMAGE_SPECFILE="$(realpath "$1")"; shift
declare    IMAGE_SPECS_PIN=ff7fdbf; [[ $# -gt 0 ]] && { IMAGE_SPECS_PIN="$1"; shift; }

prepare_system() {
    sudo apt install \
	 vmdb2 dosfstools qemu-utils qemu-user-static debootstrap binfmt-support \
	 time kpartx bmap-tools python3 zerofree apt-transport-https git fakemachine
}

main() {
    local DIR
    local IMAGE_BASENAME; IMAGE_BASENAME="$(basename "$IMAGE_SPECFILE" .vmdb2)"
    local ANSIBLE_DIR;    ANSIBLE_DIR="$(dirname "$IMAGE_SPECFILE")/ansible"
    local OVERLAY_DIR;    OVERLAY_DIR="$(dirname "$IMAGE_SPECFILE")/overlay"

    [[ $DO_PREPARE_SYSTEM -eq 0 ]] || prepare_system
    if [[ ! -d image-specs ]]; then
	mkdir image-specs
	chmod 0775 image-specs
	git clone --recursive https://salsa.debian.org/raspi-team/image-specs.git
    fi
    cd image-specs
    DIR="$(/usr/bin/pwd -P)"
    git fetch
    git switch -d "$IMAGE_SPECS_PIN"
    cp "$IMAGE_SPECFILE" ./
    cp -a "$ANSIBLE_DIR" ./ || true
    cp -a "$OVERLAY_DIR" ./ || true

    fakemachine --volume="$DIR" -- \
      env --chdir="$DIR" \
        vmdb2 \
          --verbose \
          --rootfs-tarball="$IMAGE_BASENAME.tar.gz" \
	  --output="$IMAGE_BASENAME.img" \
	  --log="$IMAGE_BASENAME.log" \
	  "$IMAGE_BASENAME.vmdb2"
    chmod 0660 "$DIR/$IMAGE_BASENAME".{img,log}
}

main "$@"

# Example output from running in a VM with 4 vCPU's and 4GB RAM (with fakemachine --backend=kvm):
# All went fine.
# 1182.81user 205.07system 18:26.11elapsed 125%CPU (0avgtext+0avgdata 2208356maxresident)k
# 2776516inputs+6262472outputs (802major+128868minor)pagefaults 0swaps
# chmod 0644 raspi_4_bookworm.img
#
# builder@debian12:~/usr/src/image-specs$ ls -lh raspi_4_bookworm.img
# -rw-r--r-- 1 builder builder 2.5G Dec 10 17:33 raspi_4_bookworm.img
