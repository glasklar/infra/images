#! /bin/bash
set -eu

. "$(dirname $0)/common.sh"
now="$(date +%s)"

apt-get install -y samba
backup /etc/samba/smb.conf "$now"
cat > /etc/samba/smb.conf <<'EOF'
[global]
  interfaces = eth0
  bind interfaces only = yes
  log file = /var/log/samba/log.%m
  max log size = 1000
  logging = file
  #panic action = /usr/share/samba/panic-action %d
  server role = standalone server
  obey pam restrictions = yes
  #unix password sync = yes
  #pam password change = yes
  map to guest = bad user
  security = user
  min protocol = NT1
  #usershare allow guests = yes
# [homes]
#   comment = Home Directories
#   browseable = no
#   directory mask = 0700
#   read only = yes
#   valid users = %S
[public]
  path = /var/lib/sambashares/iso
  public = yes
  guest account = readonly
  guest ok = yes
  #load printers = no
  printable = no
  writable = no
  force directory mode = 0666
  browseable = yes
EOF
systemctl restart samba
