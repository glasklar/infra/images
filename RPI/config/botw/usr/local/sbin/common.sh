#! /bin/bash
set -eu

backup() {
    local fn="$1"; shift
    local ts="$1"; shift

    if [ -e "$fn" ]; then
	mv "$fn" "$fn.$ts.BAK"
    fi
}
