#! /bin/bash
set -eu

. "$(dirname $0)/common.sh"
now="$(date +%s)"

wired() {
    cat > /etc/network/interfaces.d/eth0 <<EOF
allow-hotplug eth0
iface eth0 inet dhcp
iface eth0 inet6 auto
EOF
}

wifi_wpasupplicant() {
    # NOTE: Even if installation of wpasupplicant results in
    # wpa_supplicant.service being enabled, thanks to
    # /etc/wpa_supplicant/ifupdown.sh there is no need to disable
    # wpa_supplicant in order for ifup to run it.

    backup /etc/wpa_supplicant/wpa_supplicant.conf "$now"
    (umask 0077; cat > /etc/wpa_supplicant/wpa_supplicant.conf) <<EOF
ctrl_interface=DIR=/run/wpa_supplicant GROUP=netdev
pmf=1
EOF
    [ -f /etc/wpa_supplicant/wpa_supplicant-secrets.conf ] && cat /etc/wpa_supplicant/wpa_supplicant-secrets.conf >> /etc/wpa_supplicant/wpa_supplicant.conf

    cat > /etc/network/interfaces.d/wlan0 <<EOF
allow-hotplug wlan0
iface wlan0 inet manual
        wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf

# iface 'default' is used for network blocks in wpa_supplicant.conf with no id_str
iface default inet dhcp
iface default inet6 auto
EOF

wired
wifi_wpasupplicant
