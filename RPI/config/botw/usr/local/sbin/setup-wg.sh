#! /bin/bash
set -eu

# NOTE: This script only creates configuration files and does on
# purpose not attempt to reload systemd-networkd.

. "$(dirname $0)/common.sh"
now="$(date +%s)"

NAME="$1"; shift
PEERPUBKEY="$1"; shift
PEERENDPOINT="$1"; shift
TUNADDRHOSTPART="$1"; shift
# TODO: validate that all arguments are single token arguments, ie contain no spaces

if [[ -f /etc/wireguard/"$NAME".private.key &&
	  -f /etc/wireguard/"$NAME".public.key &&
	  -f /etc/wireguard/"$NAME".preshared.key ]]; then
    echo "reusing existing wg keys"
else
    (
	umask 0027
	wg genkey | tee /etc/wireguard/"$NAME".private.key | wg pubkey > /etc/wireguard/"$NAME".public.key
	wg genpsk > /etc/wireguard/"$NAME".preshared.key
    )
fi
chgrp systemd-network /etc/wireguard /etc/wireguard/"$NAME".*.key
chmod g+rX /etc/wireguard

backup 30-"$NAME".netdev "$now"
cat > /etc/systemd/network/30-"$NAME".netdev <<EOF
[NetDev]
Name=$NAME
Kind=wireguard

[WireGuard]
PrivateKeyFile=/etc/wireguard/$NAME.private.key

[WireGuardPeer]
Endpoint=$PEERENDPOINT
PublicKey=$PEERPUBKEY
PresharedKeyFile=/etc/wireguard/$NAME.preshared.key
AllowedIPs=10.47.11.1/32,fd00:47:11::1/128
AllowedIPs=10.47.11.21/32,fd00:47:11::21/128
AllowedIPs=10.47.11.35/32,fd00:47:11::35/128
PersistentKeepalive=25
EOF

backup 30-"$NAME".network "$now"
cat > /etc/systemd/network/30-"$NAME".network <<EOF
[Match]
Name=$NAME

[Network]
Address=10.47.11.${TUNADDRHOSTPART}/32
Address=fd00:47:11::${TUNADDRHOSTPART}/128

[Route]
Gateway=10.47.11.1
Destination=10.47.11.0/24
GatewayOnLink=yes

[Route]
Gateway=fd00:47:11::1
Destination=fd00:47:11::/64
GatewayOnLink=yes
EOF
