#! /bin/bash
set -eu

. "$(dirname $0)/common.sh"
now="$(date +%s)"

# NOTE: udhcpd isn't actually starting by default due to DHCPD_ENABLED=no in /etc/default/udhcpd
apt-get install -y udhcpd
backup /etc/udhcpd.conf "$now"
cat > /etc/udhcpd.conf <<'EOF'
start		172.27.0.10
end		172.27.0.30
interface	eth0
max_leases	21
option	lease	3600
EOF
(umask 0077; mkdir -p /var/lib/misc)
